# %%
from enum import Enum
import datetime


class TipoTransacaoEnum(Enum):
    ABERTURA = 0
    DEPOSITO = 1
    SAQUE = 2
    DOC_ENV = 3
    TED_ENV = 4
    DOC_REC = 31
    DOC_DEV = 39
    TED_REC = 41
    TED_DEV = 49
    ENCERRAMENTO = 99


# %%
class Transacao:
    def __init__(self, dataParam, tipoOperacaoParam: TipoTransacaoEnum, valorParam: float, observacao: str = ''):
        self._data = dataParam
        self._tipoOperacao = tipoOperacaoParam
        self._valor = valorParam
        self._observacao = observacao

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, valor):
        self._data = valor

    @property
    def tipoOperacao(self):
        return self._tipoOperacao

    @tipoOperacao.setter
    def tipoOperacao(self, valor: TipoTransacaoEnum):
        self._tipoOperacao = valor

    @property
    def valor(self):
        return self._valor

    @valor.setter
    def valor(self, valorParam):
        self._valor = valorParam

    @property
    def observacao(self):
        return self._observacao

    @observacao.setter
    def observacao(self, novoValor: str):
        self._observacao = novoValor


# %%
class Pessoa:
    def __init__(self, nomeParam: str, cpfParam: str, sexoParam: str):
        self._nome = nomeParam
        self._cpf = cpfParam
        self._sexo = sexoParam

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, valor):
        self._nome = valor

    @property
    def cpf(self):
        return self._cpf

    @cpf.setter
    def cpf(self, valor):
        self._cpf = valor

    @property
    def sexo(self):
        return self._sexo

    @sexo.setter
    def sexo(self, valor):
        self._sexo = valor


# %%
class Historico:
    def __init__(self, dataAberturaParam: datetime):
        self._dataAbertura = dataAberturaParam
        self._operacoes = []
        self.adicionarTransacao(dataAberturaParam, TipoTransacaoEnum.ABERTURA, 0)

    def adicionarTransacao(self, data, tipo: TipoTransacaoEnum, valor: float, observacao: str = ''):
        novaTransacao = Transacao(data, tipo, valor)
        self._operacoes.append(novaTransacao)

    def imprimir(self, usaTotalizadores: bool = False, dataFiltroInicio: datetime = None,
                 dataFiltroFinal: datetime = None):
        dataAnterior: datetime = None
        valor: float = 0
        for item in self._operacoes:
            printar: bool = True
            maskValue: str

            if (usaTotalizadores and dataAnterior != None and item.data.date() != dataAnterior):
                print('----------------------------------------------------')
                print('\t    Saldo em {:%d-%m-%Y}'.format(dataAnterior) + ': R$' + '{:>15.2f}'.format(valor))
                print('----------------------------------------------------')
            valor += item.valor
            dataAnterior = item.data.date()

            if (dataFiltroInicio != None):
                if (datetime.datetime.date(item.data) < datetime.datetime.date(dataFiltroInicio)):
                    printar = False
            if (dataFiltroFinal != None):
                if (datetime.datetime.date(item.data) > datetime.datetime.date(dataFiltroFinal)):
                    printar = False
            if (printar):
                maskValue = ' R$' if item.valor >= 0 else '-R$'
                print(
                    f'{item.data.date()}   {item.tipoOperacao.name}\t{item.observacao}\t{maskValue}' + '{:>15.2f}'.format(
                        abs(item.valor)))
        if (usaTotalizadores):
            print('----------------------------------------------------')
            print('\t    Saldo em {:%d-%m-%Y}'.format(dataAnterior) + ': R$' + '{:>15.2f}'.format(valor))
            print('----------------------------------------------------')


# %%
class Conta:
    def __init__(self, numeroConta: int, cliente: Pessoa, dataAbertura: datetime):
        self._numeroConta = numeroConta
        self._cliente = cliente
        self._saldo = 0
        self._limite = 0
        self._historico = Historico(dataAbertura)

    @property
    def numeroConta(self):
        return self._numeroConta

    @numeroConta.setter
    def numeroConta(self, novoValor):
        self._numeroConta = novoValor

    @property
    def cliente(self):
        return self._cliente

    @cliente.setter
    def cliente(self, novoValor: Pessoa):
        self._cliente = novoValor

    @property
    def saldo(self):
        return self._saldo

    @saldo.setter
    def saldo(self, novoValor):
        self._saldo = novoValor

    @property
    def limite(self):
        return self._limite

    @limite.setter
    def limite(self, novoValor):
        self._limite = novoValor

    @property
    def historico(self):
        return self._historico

    @historico.setter
    def historico(self, novoValor: Historico):
        self._historico = novoValor

    def totalDisponivel(self):
        return self._saldo + self._limite

    def depositar(self, valor):
        self._saldo += valor
        self._historico.adicionarTransacao(datetime.datetime.now(), TipoTransacaoEnum.DEPOSITO, valor)
        print(f'DEPÓSITO REALIZADO! R$ {valor}')

    def sacar(self, valor):
        if (self.totalDisponivel() >= valor):
            self._saldo -= valor
            self._historico.adicionarTransacao(datetime.datetime.now(), TipoTransacaoEnum.SAQUE, -valor)
            print(f'SAQUE REALIZADO! R$ {valor} | Saldo: {self._saldo}')
        else:
            print('Saldo Indisponível para saque! Saque: ' + valor + ' | Saldo: ' + self._saldo)

    def transferir(self, modo: TipoTransacaoEnum, valor: float, destino):
        if (modo == TipoTransacaoEnum.TED_ENV or modo == TipoTransacaoEnum.DOC_ENV):
            if (self.totalDisponivel() >= valor):
                self._saldo -= valor
                self._historico.adicionarTransacao(datetime.datetime.now(), modo, -valor, str(destino.numeroConta))
                if (destino != None):
                    destino.receberTransferencia(modo, self, valor)
                    print(f'TRANSFERENCIA AGENDADA! R$ {valor} | Saldo: {self._saldo}')
                else:
                    self._saldo += valor
                    modoAux: TipoTransacaoEnum
                    dataTransf: datetime
                    if (modo == TipoTransacaoEnum.DOC_ENV):
                        modoAux = TipoTransacaoEnum.DOC_DEV
                        dataTransf = datetime.datetime.now() + datetime.timedelta(days=1)
                    else:
                        modoAux = TipoTransacaoEnum.TED_DEV
                        dataTransf = datetime.datetime.now()
                    self._historico.adicionarTransacao(dataTransf, modoAux, valor, 'A Conta informada não existe.')
            else:
                print(f'Saldo Indisponível para {modo.name}! Valor: {valor} | Saldo: {self._saldo}')

    def receberTransferencia(self, modo: TipoTransacaoEnum, remetente, valor: float):
        self._saldo += valor
        modoAux: TipoTransacaoEnum
        dataTransf: datetime
        if (modo == TipoTransacaoEnum.DOC_ENV):
            modoAux = TipoTransacaoEnum.DOC_REC
            dataTransf = datetime.datetime.now() + datetime.timedelta(days=1)
        else:
            modoAux = TipoTransacaoEnum.TED_REC
            dataTransf = datetime.datetime.now()
        self._historico.adicionarTransacao(dataTransf, modoAux, valor, remetente)

    def extrato(self, dataFiltroInicio: datetime = None, dataFiltroFinal: datetime = None):
        self._historico.imprimir(True, dataFiltroInicio, dataFiltroFinal)


class Banco:
    def __init__(self):
        self._listaClientes = {}
        self._listaContas = {}
        self._listaContas

    def abrirConta(self, clienteNome: str, clienteCpf: str, clienteSexo: str, saldoAbertura: float = 0):
        numeroConta = len(self._listaContas) + 1
        if (self._listaClientes.__contains__(clienteCpf)):
            novoCliente = self._listaClientes[clienteCpf]
        else:
            novoCliente = Pessoa(clienteNome, clienteCpf, clienteSexo)
            self._listaClientes[clienteCpf] = novoCliente

        novaConta = Conta(numeroConta, novoCliente, datetime.datetime.now())
        self._listaContas[numeroConta] = novaConta

        if (saldoAbertura > 0):
            novaConta.depositar(saldoAbertura)

    def encerrarConta(self, numeroConta):
        if (self._listaContas.__contains__(numeroConta)):
            fechaConta = Conta(self._listaContas)
            if (fechaConta.saldo > 0):
                print(f'Não é possível encerrar a conta enquanto ainda tiver saldo. Saldo: {fechaConta.saldo}')
            else:
                fechaConta._historico.adicionarTransacao(datetime.datetime.now(), TipoTransacaoEnum.ENCERRAMENTO, 0)

    @property
    def contas(self):
        return self._listaContas

    @property
    def clientes(self):
        return self._listaClientes

    def quantidadeContas(self):
        return len(self._listaContas)


# %%
bancoCentral = Banco()
bancoCentral.abrirConta('Gabriel Duro', '012.606.880-11', 'Masculino')

contaGabriel = bancoCentral.contas[1]
contaGabriel.depositar(15)
contaGabriel.sacar(5)
# print(f'O saldo da Conta {contaGabriel.numeroConta} de {contaGabriel.cliente.nome} é de {contaGabriel.saldo}')
contaGabriel.extrato()

bancoCentral.abrirConta('Thais Teixeira', '111.222.333-44', 'Feminino', 1000)

contaThais = bancoCentral.contas[2]
contaGabriel.depositar(5500)
contaThais.depositar(2700)
contaThais.limite = 1000
contaThais.transferir(TipoTransacaoEnum.DOC_ENV, 380, contaGabriel)
print('\n\n')
for item in bancoCentral.contas:
    itemObj = bancoCentral.contas[item]
    print(f'Extrato da conta {itemObj.numeroConta} de {itemObj.cliente.nome}:')
    itemObj.extrato()
    print('\n')

contaGabriel.extrato(datetime.datetime.today(), datetime.datetime.today())