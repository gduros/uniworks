# %%
from enum import Enum
import datetime
import qprompt

class TipoTransacaoEnum(Enum):
    ABERTURA = 0
    DEPOSITO = 1
    SAQUE = 2
    DOC_ENV = 3
    TED_ENV = 4
    DOC_REC = 31
    DOC_DEV = 39
    TED_REC = 41
    TED_DEV = 49
    ENCERRAMENTO = 99

class TipoMoedaEnum(Enum):
    CENT005 = 0.05
    CENT010 = 0.10
    CENT025 = 0.25
    CENT050 = 0.50
    CENT100 = 1
    CEDU002 = 2
    CEDU005 = 5
    CEDU010 = 10
    CEDU020 = 20
    CEDU050 = 50
    CEDU100 = 100

class TipoSexoEnum(Enum):
    FEMININO = 1
    MASCULINO = 2
    OUTRO = 3

class TipoBuscaContaEnum(Enum):
    CLIENTE_CPF = 10
    CLIENTE_NOME = 11
    CLIENTE_ATIVO = 19
    CONTA_NUMERO = 20
    CONTA_DISPONIVEL = 22
    CONTA_ATIVO = 29

# %%
class Transacao:
    def __init__(self, dataParam, tipoOperacaoParam: TipoTransacaoEnum, valorParam: float, observacao: str = ''):
        self._data = dataParam
        self._tipoOperacao = tipoOperacaoParam
        self._valor = valorParam
        self._observacao = observacao

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, valor):
        self._data = valor

    @property
    def tipoOperacao(self):
        return self._tipoOperacao

    @tipoOperacao.setter
    def tipoOperacao(self, valor: TipoTransacaoEnum):
        self._tipoOperacao = valor

    @property
    def valor(self):
        return self._valor

    @valor.setter
    def valor(self, valorParam):
        self._valor = valorParam

    @property
    def observacao(self):
        return self._observacao

    @observacao.setter
    def observacao(self, novoValor: str):
        self._observacao = novoValor


# %%
class Pessoa:
    def __init__(self, nomeParam: str, cpfParam: str, sexoParam: TipoSexoEnum):
        self._nome = nomeParam
        self._cpf = cpfParam
        self._sexo = sexoParam

    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, valor):
        self._nome = valor

    @property
    def cpf(self):
        return self._cpf

    @cpf.setter
    def cpf(self, valor):
        self._cpf = valor

    @property
    def sexo(self):
        return self._sexo

    @sexo.setter
    def sexo(self, valor):
        self._sexo = valor

    def exibir(self):
        return f'NOME: {self._nome}\nCPF: {self._cpf}\nSEXO: {self._sexo.name}\n'

# %%
class Cliente(Pessoa):
    def __init__(self, nomeParam, cpfParam, sexoParam):
        super().__init__(nomeParam, cpfParam, sexoParam)
        self._ativo: bool = True
    
    @property
    def ativo(self):
        return self._ativo
    
    @ativo.setter
    def ativo(self, novoValor: bool):
        self._ativo = novoValor

# %%
class Historico:
    def __init__(self, dataAberturaParam: datetime):
        self._dataAbertura = dataAberturaParam
        self._operacoes = []
        self.adicionarTransacao(dataAberturaParam, TipoTransacaoEnum.ABERTURA, 0)

    def adicionarTransacao(self, data, tipo: TipoTransacaoEnum, valor: float, observacao: str = ''):
        novaTransacao = Transacao(data, tipo, valor)
        self._operacoes.append(novaTransacao)

    def imprimir(self, usaTotalizadores: bool = False, dataFiltroInicio: datetime = None,
                 dataFiltroFinal: datetime = None):
        dataAnterior: datetime = None
        valor: float = 0
        for item in self._operacoes:
            printar: bool = True
            maskValue: str

            if (usaTotalizadores and dataAnterior != None and item.data.date() != dataAnterior):
                print('----------------------------------------------------')
                print('\t    Saldo em {:%d-%m-%Y}'.format(dataAnterior) + ': R$' + '{:>15.2f}'.format(valor))
                print('----------------------------------------------------')
            valor += item.valor
            dataAnterior = item.data.date()

            if (dataFiltroInicio != None):
                if (datetime.datetime.date(item.data) < datetime.datetime.date(dataFiltroInicio)):
                    printar = False
            if (dataFiltroFinal != None):
                if (datetime.datetime.date(item.data) > datetime.datetime.date(dataFiltroFinal)):
                    printar = False
            if (printar):
                maskValue = ' R$' if item.valor >= 0 else '-R$'
                print(
                    f'{item.data.date()}   {item.tipoOperacao.name}\t{item.observacao}\t{maskValue}' + '{:>15.2f}'.format(
                        abs(item.valor)))
        if (usaTotalizadores):
            print('----------------------------------------------------')
            print('\t    Saldo em {:%d-%m-%Y}'.format(dataAnterior) + ': R$' + '{:>15.2f}'.format(valor))
            print('----------------------------------------------------')


# %%
class Conta:
    def __init__(self, numeroConta: int, cliente: Cliente, dataAbertura: datetime, senha = '123456'):
        self._numeroConta: int = numeroConta
        self._cliente: Cliente = cliente
        self._saldo: float = 0
        self._limite: float = 0
        self._historico: Historico = Historico(dataAbertura)
        self._senha: str = senha
        self._ativa: bool = True

    @property
    def ativo(self):
        return self._ativa
    
    @ativo.setter
    def ativo(self, novoValor: bool):
        self._ativa = novoValor

    @property
    def senha(self):
        return self._senha
    
    @senha.setter
    def senha(self, novoValor):
        self._senha = novoValor
    
    @property
    def numeroConta(self):
        return self._numeroConta

    @numeroConta.setter
    def numeroConta(self, novoValor):
        self._numeroConta = novoValor

    @property
    def cliente(self):
        return self._cliente

    @cliente.setter
    def cliente(self, novoValor: Cliente):
        self._cliente = novoValor

    @property
    def saldo(self):
        return self._saldo

    @saldo.setter
    def saldo(self, novoValor):
        self._saldo = novoValor

    @property
    def limite(self):
        return self._limite

    @limite.setter
    def limite(self, novoValor):
        self._limite = novoValor

    @property
    def historico(self):
        return self._historico

    @historico.setter
    def historico(self, novoValor: Historico):
        self._historico = novoValor

    def totalDisponivel(self):
        return self._saldo + self._limite

    def depositar(self, valor):
        self._saldo += valor
        self._historico.adicionarTransacao(datetime.datetime.now(), TipoTransacaoEnum.DEPOSITO, valor)
        print(f'DEPÓSITO REALIZADO! R$ {valor}')

    def sacar(self, valor):
        if (self.totalDisponivel() >= valor):
            self._saldo -= valor
            self._historico.adicionarTransacao(datetime.datetime.now(), TipoTransacaoEnum.SAQUE, -valor)
            print(f'SAQUE REALIZADO! R$ {valor} | Saldo: {self._saldo}')
        else:
            print('Saldo Indisponível para saque! Saque: ' + str(valor) + ' | Saldo: ' + str(self._saldo))

    def transferir(self, modo: TipoTransacaoEnum, valor: float, destino):
        if (modo == TipoTransacaoEnum.TED_ENV or modo == TipoTransacaoEnum.DOC_ENV):
            if (self.totalDisponivel() >= valor):
                self._saldo -= valor
                if(destino == None):
                    self._historico.adicionarTransacao(datetime.datetime.now(), modo, -valor, 'Conta Desconhecida')
                else:
                    self._historico.adicionarTransacao(datetime.datetime.now(), modo, -valor, str(destino.numeroConta))
                
                if (destino != None):
                    destino.receberTransferencia(modo, self, valor)
                    print(f'TRANSFERENCIA AGENDADA! R$ {valor} | Saldo: {self._saldo}')
                else:
                    print(f'TRANSFERENCIA AGENDADA! R$ {valor} | Saldo: {self._saldo}')
                    self._saldo += valor
                    modoAux: TipoTransacaoEnum
                    dataTransf: datetime
                    if (modo == TipoTransacaoEnum.DOC_ENV):
                        modoAux = TipoTransacaoEnum.DOC_DEV
                        dataTransf = datetime.datetime.now() + datetime.timedelta(days=1)
                    else:
                        modoAux = TipoTransacaoEnum.TED_DEV
                        dataTransf = datetime.datetime.now() + datetime.timedelta(days=1)
                    self._historico.adicionarTransacao(dataTransf, modoAux, valor, 'A Conta informada não existe.')
            else:
                print(f'Saldo Indisponível para {modo.name}! Valor: {valor} | Saldo: {self._saldo}')

    def receberTransferencia(self, modo: TipoTransacaoEnum, remetente, valor: float):
        self._saldo += valor
        modoAux: TipoTransacaoEnum
        dataTransf: datetime
        if (modo == TipoTransacaoEnum.DOC_ENV):
            modoAux = TipoTransacaoEnum.DOC_REC
            dataTransf = datetime.datetime.now() + datetime.timedelta(days=1)
        else:
            modoAux = TipoTransacaoEnum.TED_REC
            dataTransf = datetime.datetime.now()
        self._historico.adicionarTransacao(dataTransf, modoAux, valor, remetente)

    def extrato(self, dataFiltroInicio: datetime = None, dataFiltroFinal: datetime = None):
        self._historico.imprimir(True, dataFiltroInicio, dataFiltroFinal)
    
    def exibir(self):
        return f'CONTA: {self._numeroConta}\nCLIENTE: {self._cliente.exibir()}\nLIMITE: {self._limite}\nSALDO: {self._saldo}\nATIVA: {self._ativa}'

# %%
class Atendimento:
    def __init__(self):
        self._mapMoeda = dict([TipoMoedaEnum,0])
    
    def valorDisponivel(self):
        valorRetorno = 0.
        for moeda in self._mapMoeda.keys():
            moedaEnum: TipoMoedaEnum = moeda
            valorRetorno += moedaEnum.value * self._mapMoeda[moedaEnum]
        
        return valorRetorno

# class Terminal(Atendimento):
#     def __init__(self):
#         super().__init__
#         self.

# %%
class Banco:
    def __init__(self):
        self._mapClientes = {}
        self._mapContas = {}
        self._contaSelecionada: Conta

    def cadastrarCliente(self, cpfClienteParam: str = ''):
        if(cpfClienteParam == ''):
            print('As informações Necessárias para cadastro de cliente são:\n\nNOME\tCPF\tSEXO')
            cpfCliente = qprompt.ask_str(msg='Digite o CPF do Cliente: ', blk=False)
        else:
            cpfCliente = cpfClienteParam
        if(self._mapClientes.__contains__(cpfCliente)):
            novoCliente: Cliente = self._mapClientes[cpfCliente]
            if(not novoCliente.ativo):
                if(qprompt.ask_yesno('A Pessoa informada já está cadastrada, mas está inativa. Deseja reativar este cadastro? ')):
                    novoCliente.ativo = True
                    print('CADASTRO REATIVADO!')
                    return novoCliente
                else:
                    return None
            else:
                print('Cliente já cadastrado.')
                return None
        else:
            nomeCliente = qprompt.ask_str(msg='Digite o Nome Completo do Cliente: ', blk=False)
            sexoClienteStr = qprompt.ask_str(msg='Informe o Gênero do Cliente: ', vld=['M', 'F', 'O'], blk=False)
            if(sexoClienteStr == 'M'):
                sexoCliente = TipoSexoEnum.MASCULINO
            elif(sexoClienteStr == 'F'):
                sexoCliente = TipoSexoEnum.FEMININO
            else:
                sexoCliente = TipoSexoEnum.OUTRO

            novoCliente = Cliente(nomeCliente, cpfCliente, sexoCliente)
            self._mapClientes[cpfCliente] = novoCliente
            print(f'CLIENTE #{novoCliente.cpf} CADASTRADO COM SUCESSO')
            return novoCliente
    
    def alterarCliente(self):
        cpfCliente = qprompt.ask_str(msg='Informe o CPF do cliente à alterar: ', blk=False)
        if(self._mapClientes.__contains__(cpfCliente)):
            alteraCliente: Cliente = self._mapClientes[cpfCliente]
            if(not alteraCliente.ativo):
                print('O cliente selecionado está desativo:')
                print(alteraCliente.exibir())
                if(qprompt.ask_yesno('Continuar irá reativá-lo. Deseja continuar? ')):
                    alteraCliente.ativo = True
                else:
                    return False
            nomeCliente = qprompt.ask_str(msg='Digite o novo Nome (Deixe em Branco para não modificar): ')
            cpfCliente = qprompt.ask_str(msg='Digite o novo CPF (Deixe em Branco para não modificar): ')
            sexoClienteStr = qprompt.ask_str(msg='Informe o Gênero do Cliente: ', vld=['M', 'F', 'O'])
            if(sexoClienteStr == 'M'):
                sexoCliente = TipoSexoEnum.MASCULINO
            elif(sexoClienteStr == 'F'):
                sexoCliente = TipoSexoEnum.FEMININO
            elif(sexoClienteStr == 'O'):
                sexoCliente = TipoSexoEnum.OUTRO
            else:
                sexoCliente = None

            if(nomeCliente != ''):
                alteraCliente.nome = nomeCliente
            if(sexoCliente != None):
                alteraCliente.sexo = sexoCliente
            if(cpfCliente != ''):
                self._mapClientes.__delitem__(alteraCliente.cpf)
                alteraCliente.cpf = cpfCliente
                self._mapClientes[cpfCliente] = alteraCliente
            
            print('CLIENTE ALTERADO COM SUCESSO!')
            return True
        else:
            print('CLIENTE NÃO CADASTRADO.')
            return False
    
    def exibirCliente(self):
        cpfCliente = qprompt.ask_str(msg='Informe o CPF do cliente à exibir: ', blk=False)
        if(self._mapClientes.__contains__(cpfCliente)):
            exibirCliente: Cliente
            exibirCliente = self._mapClientes[cpfCliente]
            print(exibirCliente.exibir())
        else:
            print('CLIENTE NÃO CADASTRADO.')

    def inativarCliente(self):
        cpfCliente = qprompt.ask_str(msg='Informe o CPF do cliente à Inativar: ', blk=False)
        if(self._mapClientes.__contains__(cpfCliente)):
            inativarCliente: Cliente = self._mapClientes[cpfCliente]
            # Olha todas as contas para ver se está sendo usado
            listaCondicoes: list(TipoBuscaContaEnum) = []
            listaCondicoes.append(TipoBuscaContaEnum.CONTA_ATIVO)
            listaCondicoes.append(TipoBuscaContaEnum.CLIENTE_CPF)
            listaFiltros = [inativarCliente.cpf]
            listaContas: list(Conta) = self.buscarConta(listaCondicoes, listaFiltros)
            if(len(listaContas) > 0):
                print(f'Não é possível inativar o cliente pois este contém {len(listaContas)} abertas.')
            else:
                inativarCliente.ativo = False
        else:
            print('CLIENTE NÃO ENCNTRADO.')

    def abrirConta(self):
        print('ABERTURA DE CONTAS\n\nPara Abrir contas, tenha em mãos o documento do Cliente')
        clienteCpf = qprompt.ask_str(msg='Digite o CPF do cliente: ', blk=False)
        numeroConta = len(self._mapContas) + 1
        if (self._mapClientes.__contains__(clienteCpf)):
            novoCliente = self._mapClientes[clienteCpf]
            if(novoCliente.ativo):
                print(f'Cliente encontrado: {novoCliente.cpf} - {novoCliente.nome}')
            else:
                print(f'O cliente {novoCliente.cpf} - {novoCliente.nome} foi encontrado, mas está inativado.')
                return False
        else:
            print('Cliente não cadastrado, informe os dados adicionais: ')
            novoCliente = self.cadastrarCliente(cpfClienteParam=clienteCpf)

        novaConta = Conta(numeroConta, novoCliente, datetime.datetime.now())
        self._mapContas[numeroConta] = novaConta

        saldoAbertura = qprompt.ask_float(msg=f'Conta {numeroConta} cadastrada com Sucesso. Digite o saldo de Abertura, se achar necessário:', dft=0.0)

        if (saldoAbertura > 0):
            novaConta.depositar(saldoAbertura)
        
        senhaConta = qprompt.ask_str(msg='Deseja definir uma senha para a conta neste momento?', dft='123456', blk=False)
        novaConta.senha = senhaConta
    
    def alterarConta(self):
        numeroConta = qprompt.ask_int('Informe o numero da conta à alterar: ')
        if(self._mapContas.__contains__(numeroConta)):
            alteraConta = self._mapContas[numeroConta]
            if(alteraConta.ativa):
                novoLimite = qprompt.ask_float('Digite o novo Limite da Conta (Deixe em Branco para não alterar): ')
                novoSenha = qprompt.ask_str(msg='Digite a nova Senha da Conta (Deixe em Branco para não alterar): ')
                novoClienteCpf = qprompt.ask_str(msg='Digite o CPF do novo Responsável da Conta (Deixe em Branco para não alterar): ')
                if(novoClienteCpf != ''):
                    if(self._mapClientes.__contains__(novoClienteCpf)):
                        novoCliente = self._mapClientes[novoClienteCpf]
                    else:
                        print('Não foi possível alterar Cliente pois o cliente informado não está cadastrado.')
                        novoCliente = None
                else:
                    novoCliente = None
                if(novoLimite != 0.0):
                    alteraConta.limite = novoLimite
                if(novoSenha != ''):
                    alteraConta.senha = novoSenha
                if(novoCliente != None):
                    alteraConta.cliente = novoCliente
                
                print('CONTA ALTERADA COM SUCESSO')
                return True
            else:
                print('A Conta selecionada está ENCERRADA')
                return False

    def exibirConta(self):
        numeroConta = qprompt.ask_int('Informe o numero da conta à alterar: ')
        if(self._mapContas.__contains__(numeroConta)):
            exibirConta: Conta
            exibirConta = self._mapContas[numeroConta]
            print(exibirConta.exibir())
        else:
            print('CONTA NÃO CADASTRADA.')

    def buscarConta(self, listaCondicoes: list(TipoBuscaContaEnum), listaFiltros: list()):
        listaContasFiltradas: list(Conta) = []
        listaContasRetirar: list(Conta) = []

        for condicao in listaCondicoes:
            for conta in self._mapContas.values():
                if(condicao == TipoBuscaContaEnum.CLIENTE_ATIVO and not conta.cliente.ativo):
                    listaContasRetirar.append(conta)
                elif(condicao == TipoBuscaContaEnum.CLIENTE_CPF and not listaFiltros.__contains__(conta.cliente.cpf)):
                    listaContasRetirar.append(conta)
                elif(condicao == TipoBuscaContaEnum.CLIENTE_NOME and not listaFiltros.__contains__(conta.cliente.nome)):
                    listaContasRetirar.append(conta)
                elif(condicao == TipoBuscaContaEnum.CONTA_ATIVO and not conta.ativo):
                    listaContasRetirar.append(conta)
                elif(condicao == TipoBuscaContaEnum.CONTA_NUMERO and not listaFiltros.__contains__(conta.numeroConta)):
                    listaContasRetirar.append(conta)
        
        for conta in self._mapContas.values():
            if(not listaContasRetirar.__contains__(conta)):
                listaContasFiltradas.append(conta)
        return listaContasFiltradas
    
    def buscarCliente(self, listaCondicoes: list(TipoBuscaContaEnum), listaFiltros: list()):
        listaClienteFiltradas: list(Cliente) = []
        listaClienteRetirar: list(Cliente) = []

        for condicao in listaCondicoes:
            for cliente in self._mapClientes.values():
                if(condicao == TipoBuscaContaEnum.CLIENTE_ATIVO and not cliente.ativo):
                    listaClienteRetirar.append(cliente)
                elif(condicao == TipoBuscaContaEnum.CLIENTE_CPF and not listaFiltros.__contains__(cliente.cpf)):
                    listaClienteRetirar.append(cliente)
                elif(condicao == TipoBuscaContaEnum.CLIENTE_NOME and not listaFiltros.__contains__(cliente.nome)):
                    listaClienteRetirar.append(cliente)
        
        for cliente in self._mapClientes.values():
            if(not listaClienteRetirar.__contains__(cliente)):
                listaClienteFiltradas.append(cliente)
        return listaClienteFiltradas

    def encerrarConta(self):
        numeroConta = qprompt.ask_int('Informe o número da Conta que deseja Encerrar: ')
        if (self._mapContas.__contains__(numeroConta)):
            fechaConta = self._mapContas[numeroConta]
            if (fechaConta.saldo > 0):
                print(f'Não é possível encerrar a conta enquanto ainda tiver saldo. Saldo: {fechaConta.saldo}')
            elif(fechaConta.saldo < 0):
                print(f'Não é possível encerrar a conta enquanto houverem débitos. Saldo: {fechaConta.saldo}')
            else:
                fechaConta._historico.adicionarTransacao(datetime.datetime.now(), TipoTransacaoEnum.ENCERRAMENTO, 0)
                fechaConta.ativa = False
                print('Conta encerrada com sucesso.')
        else:
            print('Não foi possível encerrar a conta pois não foi encontrada.')
    
    def selecionarConta(self, exigeSenha: bool = True):
        print('Digite o número da Conta que deseja acessar e a senha.')
        numeroConta = qprompt.ask_int('Número da Conta: ')
        if( exigeSenha):
            senhaConta = qprompt.ask_str(msg='Senha de Acesso: ', blk=False)

        if( self._mapContas.__contains__(numeroConta)):
            if( self._mapContas[numeroConta].senha == senhaConta or not exigeSenha ):
                self._contaSelecionada = self._mapContas[numeroConta]
                return self._mapContas[numeroConta]
            else:
                print('Senha incorreta.')
                return None
        else:
            print('Não foi encontrada conta com este número.')
            return None

    def sacarDeConta(self):
        if(self._contaSelecionada != None):
            print(f'Conectado à conta #{self._contaSelecionada.numeroConta}')
            valor = qprompt.ask_float('Valor à sacar: ')
            self._contaSelecionada.sacar(valor)
            menuBounceConta.show()

    def depositarEmConta(self):
        if(self._contaSelecionada != None):
            print(f'Conectado à conta #{self._contaSelecionada.numeroConta}')
            valor = qprompt.ask_float('Valor à Depositar: ')
            self._contaSelecionada.sacar(valor)
            self._contaSelecionada.depositar(valor)
            menuBounceConta.show()

    def transferirParaConta(self):
        if(self._contaSelecionada != None):
            print(f'Conectado à conta #{self._contaSelecionada.numeroConta}')
            numeroConta = qprompt.ask_int('Conta de Destino: ')
            valor = qprompt.ask_float('Valor a enviar: ')
            modoTransfStr = qprompt.ask_str(msg='Modalidade de Envio: ', vld=['DOC', 'TED'], blk=False)
            if(modoTransfStr == 'DOC'):
                modoTransf = TipoTransacaoEnum.DOC_ENV
            else:
                modoTransf = TipoTransacaoEnum.TED_ENV
            
            if(self._mapContas.__contains__(numeroConta)):
                contaDestino: Conta = self._mapContas[numeroConta]
                self._contaSelecionada.transferir(modoTransf, valor, contaDestino)
            else:
                self._contaSelecionada.transferir(modoTransf, valor, None)
            
        menuBounceConta.show()

    def emitirExtrato(self):
        if(self._contaSelecionada != None):
            menuLocal = qprompt.Menu()
            menuLocal.add('1', 'Extrato Diário')
            menuLocal.add('2', 'Extrato Mensal')
            menuLocal.add('3', 'Extrato Completo')
            menuLocal.add('4', 'Extrato Por Período')
            menuLocal.add('0', 'Voltar')
            choice = menuLocal.show()

            if(choice == '1'):
                print('EXTRATO DE CONTA:\n=============')
                self._contaSelecionada.extrato(datetime.datetime.today(), datetime.datetime.today())
            elif(choice == '2'):
                dataHoje = datetime.datetime.today()
                dataFiltroInicio = datetime.datetime(year=dataHoje.year,month=dataHoje.month, day=1)
                dataFiltroFim = datetime.datetime(year=dataHoje.year,month=dataHoje.month+1, day=1)
                print('EXTRATO DE CONTA:\n=============')
                self._contaSelecionada.extrato(dataFiltroInicio, dataFiltroFim)
            elif(choice == '3'):
                print('EXTRATO DE CONTA:\n=============')
                self._contaSelecionada.extrato()
            elif(choice == '4'):
                dataFiltroInicio = datetime.datetime(day=int(input('Digite o Dia da Data de Início: ')), month=int(input('Digite o Mês da Data de Início: ')), year=int(input('Digite o Ano da Data de Início: ')))
                dataFiltroFim = datetime.datetime(day=int(input('Digite o Dia da Data de Final: ')), month=int(input('Digite o Mês da Data de Final: ')), year=int(input('Digite o Ano da Data de Final: ')))
                print('EXTRATO DE CONTA:\n=============')
                self._contaSelecionada.extrato(dataFiltroInicio, dataFiltroFim)
            menuBounceConta.show()

    
    @property
    def contas(self):
        return self._mapContas

    @property
    def clientes(self):
        return self._mapClientes
    
    @property
    def contaSelecionada(self):
        return self._contaSelecionada

    @contaSelecionada.setter
    def contaSelecionada(self, novoValor: Conta):
        self._contaSelecionada = Conta

    def quantidadeContas(self):
        return len(self._mapContas)
# %%

# Métodos Gerais dos Menus

def selecionarContaMenu(atmMode: bool = True):
    contaSelecionada = bancoCentral.selecionarConta(atmMode)
    if(contaSelecionada == None):
        # menuMaster.show()
        menuSlave.show()
    elif(atmMode):
        menuTAA.show()
    else:
        menuCP.show()

def buscarClientesNoBanco(tipoBusca: TipoBuscaContaEnum):
    filtro = list()
    if(tipoBusca == 
    TipoBuscaContaEnum.CLIENTE_NOME):
        filtro.append(qprompt.ask_str(msg='Digite o Nome do Cliente à pesquisar: ', blk=False))
    elif(tipoBusca == TipoBuscaContaEnum.CLIENTE_CPF):
        filtro.append(qprompt.ask_str(msg='Digite o CPF do Cliente à pesquisar: ', blk=False))
    elif(tipoBusca == TipoBuscaContaEnum.CLIENTE_ATIVO):
        pass
    else:
        print('Opção de Filtro Inválida.')
        return False
    
    listaClientes: list(Cliente) = bancoCentral.buscarCliente([tipoBusca], filtro)
    for cliente in listaClientes:
        print(cliente.exibir())
        return True

def buscarContasNoBanco(tipoBusca: TipoBuscaContaEnum):
    filtro = list()
    if(tipoBusca == TipoBuscaContaEnum.CLIENTE_NOME):
        filtro.append(qprompt.ask_str(msg='Digite o Nome do Cliente à pesquisar: ', blk=False))
    elif(tipoBusca == TipoBuscaContaEnum.CLIENTE_CPF):
        filtro.append(qprompt.ask_str(msg='Digite o CPF do Cliente à pesquisar: ', blk=False))
    elif(tipoBusca == TipoBuscaContaEnum.CONTA_NUMERO):
        filtro.append(qprompt.ask_int('Digite o número da Conta à pesquisar: '))
    elif(tipoBusca == TipoBuscaContaEnum.CONTA_ATIVO):
        pass
    elif(tipoBusca == TipoBuscaContaEnum.CLIENTE_ATIVO):
        pass
    else:
        print('Opção de Filtro Inválida.')
        return False
    
    listaContas: list(Conta) = bancoCentral.buscarConta([tipoBusca], filtro)
    for conta in listaContas:
        print(conta.exibir())
        return True

# %%

# Estrutura de Menus
bancoCentral = Banco()

menuSlave = qprompt.Menu()
menuConfig = qprompt.Menu()
menuMaster = qprompt.Menu()
menuGerente = qprompt.Menu()
menuTAA = qprompt.Menu()
menuCP = qprompt.Menu()
menuBounceConta = qprompt.Menu()

subMenuCliente = qprompt.Menu()
subMenuConta = qprompt.Menu()
subMenuClienteBusca = qprompt.Menu()
subMenuContaBusca = qprompt.Menu()

subMenuClienteBusca.add('1', 'Por Nome', buscarClientesNoBanco, [TipoBuscaContaEnum.CLIENTE_NOME])
subMenuClienteBusca.add('2', 'Por CPF', buscarClientesNoBanco, [TipoBuscaContaEnum.CLIENTE_CPF])
subMenuClienteBusca.add('3', 'Ativos', buscarClientesNoBanco, [TipoBuscaContaEnum.CLIENTE_ATIVO])
subMenuClienteBusca.add('0', 'Voltar', subMenuCliente.show)

subMenuContaBusca.add('1', 'Por Nome do Cliente', buscarContasNoBanco, [TipoBuscaContaEnum.CLIENTE_NOME])
subMenuContaBusca.add('2', 'Por CPF do Cliente', buscarContasNoBanco, [TipoBuscaContaEnum.CLIENTE_CPF])
subMenuContaBusca.add('3', 'Por Número da Conta', buscarContasNoBanco, [TipoBuscaContaEnum.CONTA_NUMERO])
subMenuContaBusca.add('3', 'Ativas', buscarContasNoBanco, [TipoBuscaContaEnum.CONTA_ATIVO])
subMenuContaBusca.add('0', 'Voltar', subMenuConta.show)

subMenuCliente.add('1', 'Cadastrar Cliente', bancoCentral.cadastrarCliente)
subMenuCliente.add('2', 'Exibir Cliente', bancoCentral.exibirCliente)
subMenuCliente.add('3', 'Alterar Cliente', bancoCentral.alterarCliente)
subMenuCliente.add('4', 'Inativar Cliente', bancoCentral.inativarCliente)
subMenuCliente.add('5', 'Buscar Clientes', subMenuClienteBusca.show)
subMenuCliente.add('0', 'Voltar', menuGerente.show)

subMenuConta.add('1', 'Cadastrar Conta', bancoCentral.abrirConta)
subMenuConta.add('2', 'Exibir Conta', bancoCentral.exibirConta)
subMenuConta.add('3', 'Alterar Conta', bancoCentral.alterarConta)
subMenuConta.add('4', 'Encerramento de Conta', bancoCentral.encerrarConta)
subMenuConta.add('5', 'Buscar Contas', subMenuContaBusca.show)
subMenuConta.add('0', 'Voltar', menuGerente.show)

menuBounceConta.add('1', 'Realizar Outra Operação', menuTAA.show)
menuBounceConta.add('0', 'Finalizar Acesso', menuSlave.show)

menuTAA.add('1', 'Saques', bancoCentral.sacarDeConta)
menuTAA.add('2', 'Depósitos', bancoCentral.depositarEmConta)
menuTAA.add('3', 'Transferencias', bancoCentral.transferirParaConta)
menuTAA.add('4', 'Extrato', bancoCentral.emitirExtrato)
menuTAA.add('0', 'Voltar', menuSlave.show)

menuCP.add('1', 'Saques', bancoCentral.sacarDeConta)
menuCP.add('2', 'Depósitos', bancoCentral.depositarEmConta)
menuCP.add('3', 'Transferencias', bancoCentral.transferirParaConta)
menuCP.add('4', 'Extrato', bancoCentral.emitirExtrato)
menuCP.add('0', 'Voltar', menuSlave.show)

menuGerente.add('1', 'Gerenciamento de Clientes', subMenuCliente.show)
menuGerente.add('2', 'Gerenciamento de Conta', subMenuConta.show)
menuGerente.add('0', 'Voltar', menuSlave.show)

menuSlave.add('1', 'Gerente de Contas', menuGerente.show)
menuSlave.add('2', 'Terminal de Auto-Atendimento', selecionarContaMenu, True)
menuSlave.add('3', 'Caixa Presencial', selecionarContaMenu, False)
menuSlave.add('0', 'Sair')

menuConfig.add('1', 'DEFINIR CÉDULAS DISPONÍVEIS EM TAA')
menuConfig.add('2', 'DEFINIR VALORES DISPONÍVEIS EM CAIXA')

menuMaster.add('1', 'INICIAR ACESSO AO BANCO', menuSlave.show)
menuMaster.add('2', 'CARREGAR TIPOS DE SAQUE', menuConfig.show)

teste: str
teste.upper()

# %%

def main():
    # %%

    ### INICIA O SISTEMA PARA O USUÁRIO ###

    print('Iniciando o Sistema de Bancos de OruData Soluções de Informática [DEMO version]: ')

    while menuSlave.show() != '0':
        pass
    print('Obrigado por utilizar nossos Serviços!')

#%%
main()