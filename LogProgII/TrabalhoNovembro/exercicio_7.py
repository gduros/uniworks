class ConjuntoInteiros:
    def __init__(self, valor_maximo):
        self._elementos = []
        self._valor_maximo = valor_maximo
        for elemento in range(0, valor_maximo):
            self.elementos.append(False)
    
    @property
    def maximo(self):
        return self._valor_maximo
    
    @property
    def elementos(self):
        return self._elementos
    
    def add(self, valor: int):
        if(valor < 0 or valor > self._valor_maximo):
            print(f'Só são permitidos valores entre 0 e {self._valor_maximo}.')
        elif(self._elementos[valor]):
            print('Valor já consta no conjunto')
        else:
            self._elementos[valor] = True
            print('Valor adicionado ao conjunto.')
    
    def remove(self, valor:int):
        if(valor < 0 or valor > self._valor_maximo):
            print('O valor não consta neste conjunto.')
        elif(not self._elementos[valor]):
            print('O valor não consta neste conjunto.')
        else:
            self._elementos[valor] = False
            print('Valor retirado do conjunto.')
    
    def __str__(self):
        valor_str: str = ''
        for elemento in range(0, self._valor_maximo):
            if self._elementos[elemento]:
                valor_str += str(elemento) + ', '
        return '{ ' + valor_str[0:len(valor_str)-2] + ' }'
    
    def uniao(self, conjunto_extra):
        if isinstance(conjunto_extra, ConjuntoInteiros):
            valor_maior = self._valor_maximo if self._valor_maximo > conjunto_extra.maximo else conjunto_extra.maximo
            conjunto_novo = ConjuntoInteiros(valor_maior)
            for elemento in range(0, valor_maior):
                unido: bool = False
                if(self.maximo >= elemento):
                    unido = True if self.elementos[elemento] else unido
                if(conjunto_extra.maximo >= elemento):
                    unido = True if conjunto_extra.elementos[elemento] else unido
                conjunto_novo.elementos[elemento] = unido
            
            return conjunto_novo
        else:
            return None
    
    def interseccao(self, conjunto_extra):
        if isinstance(conjunto_extra, ConjuntoInteiros):
            valor_maior = self._valor_maximo if self._valor_maximo > conjunto_extra.maximo else conjunto_extra.maximo
            conjunto_novo = ConjuntoInteiros(valor_maior)
            for elemento in range(0, valor_maior):
                unido: bool = False
                if(self.maximo >= elemento):
                    unido = True if self.elementos[elemento] else False
                    if not unido:
                        continue
                if(conjunto_extra.maximo >= elemento):
                    unido = True if conjunto_extra.elementos[elemento] else False
                conjunto_novo.elementos[elemento] = unido
            
            return conjunto_novo
        else:
            return None
    
    def diferenca(self, conjunto_extra):
        if isinstance(conjunto_extra, ConjuntoInteiros):
            valor_maior = self._valor_maximo if self._valor_maximo > conjunto_extra.maximo else conjunto_extra.maximo
            conjunto_novo = ConjuntoInteiros(valor_maior)
            for elemento in range(0, valor_maior):
                unido: bool = False
                if(self.maximo >= elemento):
                    unido = True if self.elementos[elemento] else False
                    if not unido:
                        continue
                if(conjunto_extra.maximo >= elemento):
                    unido = False if conjunto_extra.elementos[elemento] else True
                conjunto_novo.elementos[elemento] = unido
            
            return conjunto_novo
        else:
            return None

conjunto_A = ConjuntoInteiros(100)
conjunto_B = ConjuntoInteiros(100)
for elemento in range(0, 50):
    if elemento % 2 == 0:
        conjunto_A.add(elemento)
    if elemento % 3 == 0:
        conjunto_B.add(elemento)
    if elemento % 4 == 0:
        conjunto_B.add(elemento)
print('Conjunto A: ' + conjunto_A.__str__())
print('Conjunto B: ' + conjunto_B.__str__())
conjunto_Intersecao = conjunto_A.interseccao(conjunto_B)
print('Conjunto I: ' + conjunto_Intersecao.__str__())
conjunto_Diferenca = conjunto_A.diferenca(conjunto_B)
print('Conjunto D: ' + conjunto_Diferenca.__str__())
conjunto_Uniao = conjunto_A.uniao(conjunto_B)
print('Conjunto U: ' + conjunto_Uniao.__str__())