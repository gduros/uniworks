import math

class Complexos:
    def __init__(self, real = None, imaginario = None):
        self._real = real
        self._imaginario = imaginario

    @property
    def real(self):
        return self._real

    @real.setter
    def real(self, valor):
        self._real = valor

    @property
    def imaginario(self):
        return self._imaginario

    @imaginario.setter
    def imaginario(self, valor):
        self._imaginario = valor
    
    def modulo(self):
        return math.sqrt((self._real*self._real)+(self._imaginario*self._imaginario))
    
    def modulo_quadrado(self):
        return (self._real*self._real)+(self._imaginario*self._imaginario)

    def argumento(self):
        return math.acos(self._real/self.modulo())
    
    def add(self, numero_complexo):
        if isinstance(numero_complexo, Complexos):
            return Complexos(self._real + numero_complexo.real, self._imaginario + numero_complexo.imaginario)
        else:
            return None
    
    def multiply(self, numero_complexo):
        if isinstance(numero_complexo, Complexos):
            return Complexos(self.real*numero_complexo.real - self.imaginario*numero_complexo.imaginario, self.imaginario*numero_complexo.real + self.real*numero_complexo.imaginario)
        else:
            return None
    
    def subtract(self, numero_complexo):
        if isinstance(numero_complexo, Complexos):
            return self.add(numero_complexo.inverso_aditivo())
        else:
            return None

    def conjugado(self):
        return Complexos(self.real, self.imaginario*-1)

    def inverso_aditivo(self):
        return Complexos(real=self.real*-1,imaginario=self.imaginario*-1)

    def inverso_multiplicativo(self):
        modulo_quadrado = self.modulo_quadrado()
        return Complexos(real=self.conjugado().real/(modulo_quadrado), imaginario=self.conjugado().imaginario/(modulo_quadrado))
    
    def __str__(self):
        return f'( {self.real}, {self.imaginario} )'

teste_a = Complexos(4, 5)
teste_b = Complexos(-1, 3)
print(f'Complexo A: {teste_a.__str__()}')
print(f'Complexo B: {teste_b.__str__()}')
print(f'Soma A + B: {teste_a.add(teste_b).__str__()}')
print(f'Diferença A - B: {teste_a.subtract(teste_b).__str__()}')
print(f'Diferença A + ¬B: {teste_a.add(teste_b.inverso_aditivo()).__str__()}')
print(f'Diferença A + ¬A: {teste_a.add(teste_a.inverso_aditivo()).__str__()}')
print(f'Divisão A / ¬A: {teste_a.multiply(teste_a.inverso_multiplicativo()).__str__()}')