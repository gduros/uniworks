from enum import Enum

class TipoJogador(Enum):
    JOGADOR_CIRCULO = 'O'
    JOGADOR_XIS = 'X'
    JOGADOR_NULL = ' '
    JOGADOR_EMPATE = 'E'

class JogoDaVelha:
    def __init__(self):
        self.reset()
    
    def reset(self):
        self._grade = [[TipoJogador.JOGADOR_NULL.value, TipoJogador.JOGADOR_NULL.value, TipoJogador.JOGADOR_NULL.value], [TipoJogador.JOGADOR_NULL.value, TipoJogador.JOGADOR_NULL.value, TipoJogador.JOGADOR_NULL.value], [TipoJogador.JOGADOR_NULL.value, TipoJogador.JOGADOR_NULL.value, TipoJogador.JOGADOR_NULL.value]]
        self._jogadores = {}

    def printa_grade(self):
        print(f' {self._grade[0][0]} | {self._grade[0][1]} | {self._grade[0][2]} ')
        print(f'---|---|---')
        print(f' {self._grade[1][0]} | {self._grade[1][1]} | {self._grade[1][2]} ')
        print(f'---|---|---')
        print(f' {self._grade[2][0]} | {self._grade[2][1]} | {self._grade[2][2]} ')

    def cria_jogadores(self):
        circulo: str = input('Digite o nome do jogador do Cículo: ')
        xis: str = input('Digite o nome do jogador do Xis: ')
        self._jogadores[TipoJogador.JOGADOR_CIRCULO] = circulo
        self._jogadores[TipoJogador.JOGADOR_XIS] = xis
    
    def checa_vitoria(self):
        if(self._grade[0][0] != TipoJogador.JOGADOR_NULL.value):
            if(self._grade[0][0] == self._grade[0][1] == self._grade[0][2]):
                return self._grade[0][0]
            if(self._grade[0][0] == self._grade[1][1] == self._grade[2][2]):
                return self._grade[0][0]
            if(self._grade[0][0] == self._grade[1][0] == self._grade[2][0]):
                return self._grade[0][0]
        if(self._grade[1][0] != TipoJogador.JOGADOR_NULL.value and (self._grade[1][0] == self._grade[1][1] == self._grade[1][2])):
            return self._grade[1][0]
        if(self._grade[2][0] != TipoJogador.JOGADOR_NULL.value):
            if(self._grade[2][0] == self._grade[2][1] == self._grade[2][2]):
                return self._grade[2][0]
            if(self._grade[2][0] == self._grade[1][1] == self._grade[0][2]):
                return self._grade[2][0]
        if(self._grade[0][1] != TipoJogador.JOGADOR_NULL.value and (self._grade[0][1] == self._grade[1][1] == self._grade[2][1])):
            return self._grade[0][1]
        if(self._grade[0][2] != TipoJogador.JOGADOR_NULL.value and (self._grade[0][2] == self._grade[1][2] == self._grade[2][2])):
            return self._grade[0][2]
        if(self._grade[0][0] != TipoJogador.JOGADOR_NULL.value and self._grade[0][1] != TipoJogador.JOGADOR_NULL.value and self._grade[0][2] != TipoJogador.JOGADOR_NULL.value and self._grade[1][0] != TipoJogador.JOGADOR_NULL.value and self._grade[1][1] != TipoJogador.JOGADOR_NULL.value and self._grade[1][2] != TipoJogador.JOGADOR_NULL.value and self._grade[2][0] != TipoJogador.JOGADOR_NULL.value and self._grade[2][1] != TipoJogador.JOGADOR_NULL.value and self._grade[2][2] != TipoJogador.JOGADOR_NULL.value):
            return TipoJogador.JOGADOR_EMPATE.value
        return TipoJogador.JOGADOR_NULL.value
    
    def faz_jogada(self, jogador: TipoJogador):
        valido: bool = False
        x_input: str = ''
        y_input: str = ''
        while not valido:
            while x_input == '':
                x_input = input('Qual linha será a jogada? ')
                if x_input != '1' and x_input != '2' and x_input != '3':
                    print('Linha inválida.')
                    x_input = ''
                else:
                    x_input = str(int(x_input)-1)
            while y_input == '':
                y_input = input('Qual coluna será a jogada? ')
                if y_input != '1' and y_input != '2' and y_input != '3':
                    print('Linha inválida.')
                    y_input = ''
                else:
                    y_input = str(int(y_input)-1)
            x_num = int(x_input)
            y_num = int(y_input)
            if(self._grade[x_num][y_num] == TipoJogador.JOGADOR_NULL.value):
                self._grade[x_num][y_num] = jogador.value
                return True
            else:
                print('Este lugar já está preenchido. Escolha outro: ')
                valido = False
                x_input = ''
                y_input = ''
    
    def comeca_jogo(self):
        jogador_atual: TipoJogador = TipoJogador.JOGADOR_XIS
        jogador_vitoria = TipoJogador.JOGADOR_NULL.value
        self.reset()
        print('Começando um novo Jogo da Velha!')
        print('Vamos definir os Jogadores: ')
        self.cria_jogadores()
        
        while jogador_vitoria == TipoJogador.JOGADOR_NULL.value:
            if( jogador_atual == TipoJogador.JOGADOR_XIS):
                jogador_atual = TipoJogador.JOGADOR_CIRCULO
            else:
                jogador_atual = TipoJogador.JOGADOR_XIS
            self.printa_grade()
            print(f'É a vez de : {self._jogadores[jogador_atual]}')
            self.faz_jogada(jogador_atual)
            jogador_vitoria = self.checa_vitoria()
        self.printa_grade()
        if(jogador_vitoria == TipoJogador.JOGADOR_EMPATE.value):
            print('Ocorreu um empate!')
        else:
            print(f'O jogador {self._jogadores[jogador_atual]} venceu!')


novo_teste = JogoDaVelha()
novo_teste.comeca_jogo()