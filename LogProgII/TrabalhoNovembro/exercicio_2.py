class Fatura:
    def __init__(self, codigo: int, descricao: str, quantidade: int, preco_unitario: float):
        if( quantidade <= 0 ):
            quantidade = 0
        if( preco_unitario <= 0. ):
            preco_unitario = 0.
        self._cod: int = codigo
        self._descricao: str = descricao
        self._quantidade: int = quantidade
        self._preco: float = preco_unitario

    @property
    def codigo(self):
        return self._cod
    
    @codigo.setter
    def codigo(self, valor):
        self._cod = valor
    
    @property
    def descricao(self):
        return self._descricao
    
    @descricao.setter
    def descricao(self, valor):
        self._descricao = valor

    @property
    def quantidade(self):
        return self._quantidade
    
    @quantidade.setter
    def quantidade(self, valor):
        self._quantidade = valor

    @property
    def valor_unitario(self):
        return self._preco
    
    @valor_unitario.setter
    def valor_unitario(self, valor):
        self._preco = valor

    def get_valor_fatura(self):
        return float(self.quantidade*self.valor_unitario)

novo_teste = Fatura(1, 'Teste', 5, 35.9)
print(novo_teste.get_valor_fatura())

novo_teste0 = Fatura(2, 'Teste Qnt negativa', -5, 35.9)
print(novo_teste0.get_valor_fatura())

novo_teste00 = Fatura(3, 'Teste valor negativo', 5, -35.9)
print(novo_teste00.get_valor_fatura())