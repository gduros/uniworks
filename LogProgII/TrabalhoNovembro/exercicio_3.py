class Empregado:
    def __init__(self, nome:str, sobrenome: str, salario_mensal: float):
        if( salario_mensal <= 0. ):
            salario_mensal = 0.0
        self._primeiro_nome: str = nome
        self._sobrenome: str = sobrenome
        self._salario_mensal: float = salario_mensal

    @property
    def primeiro_nome(self):
        return self._primeiro_nome

    @primeiro_nome.setter
    def primeiro_nome(self, valor: str):
        self._primeiro_nome = valor
    
    @property
    def sobrenome(self):
        return self._sobrenome

    @sobrenome.setter
    def sobrenome(self, valor: str):
        self._sobrenome = valor

    @property
    def salario_mensal(self):
        return self._salario_mensal

    @salario_mensal.setter
    def salario_mensal(self, valor: float):
        if( valor <= 0. ):
            valor = 0.0
        self._salario_mensal = valor

    def da_aumento_por_percentual(self, percentual_aumento: float):
        if( percentual_aumento <= 0. ):
            return False
        else:
            self.salario_mensal *= (1+(percentual_aumento/100))
            return True

novo_teste = Empregado('Gabriel', 'Duro', 5000.)
print(f'Salário antes do Aumento: {novo_teste.salario_mensal}')
novo_teste.da_aumento_por_percentual(10)
print(f'Salário depois do Aumento: {novo_teste.salario_mensal}')