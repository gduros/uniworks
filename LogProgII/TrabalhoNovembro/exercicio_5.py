import datetime
class Data:
    def __init__(self):
        self._dia: int = datetime.datetime.now().day
        self._mes: int = datetime.datetime.now().month
        self._ano: int = datetime.datetime.now().year

    @property
    def dia(self):
        return self._dia
    
    @dia.setter
    def dia(self, valor):
        if( valor > self.dias_no_mes() or valor <= 0 ):
            print('Dia inválido.')
            return False
        self._dia = valor

    @property
    def mes(self):
        return self._mes
    
    @mes.setter
    def mes(self, valor):
        if(valor > 12 or valor <= 0):
            print('Mês inválido.')
            return False
        self._mes = valor
        if(self._dia > self.dias_no_mes()):
            self._dia = self.dias_no_mes()
            print('O dia foi alterado para um dia compatível com o mês selecionado.')

    @property
    def ano(self):
        return self._ano
    
    @ano.setter
    def ano(self, valor):
        if(valor <= 1900):
            valor = 1900
        self._ano = valor
        self.mes = self._mes

    def dias_no_mes(self):
        if(self.mes == 1 or self.mes == 3 or self.mes == 5 or self.mes == 7 or self.mes == 8 or self.mes == 10 or self.mes == 12):
            return 31
        elif(self.mes == 2):
            if(self.ano % 400 == 0):
                return 29
            elif(self.ano % 4 == 0 and self.ano % 100 != 0):
                return 29
            else:
                return 28
        else:
            return 30
    
    def proximo_dia(self):
        if(self.dia == self.dias_no_mes()):
            self.dia = 1
            if(self.mes == 12):
                self.mes = 1
                self.ano += 1
            else:
                self.mes += 1
        else:
            self.dia += 1
    
    def __str__(self):
        return f'{self.dia:02d}/{self.mes:02d}/{self.ano}'

    

novo_teste = Data()
print(novo_teste.__str__())
novo_teste.proximo_dia()
print(novo_teste.__str__())
novo_teste.mes = 13
novo_teste.mes = -1
print(novo_teste.__str__())
novo_teste.dia = 32
novo_teste.dia = -1
novo_teste.dia = 31
print(novo_teste.__str__())
novo_teste.dia = 30
print(novo_teste.__str__())
novo_teste.mes = 2
print(novo_teste.__str__())
novo_teste.dia = 29
novo_teste.ano = 2020
print(novo_teste.__str__())
novo_teste.dia = 29
print(novo_teste.__str__())
novo_teste.ano = -1
print(novo_teste.__str__())